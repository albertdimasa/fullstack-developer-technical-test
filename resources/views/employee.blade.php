@extends('layout.app')

@section('css')
    <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection
@section('breadcrumb')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Karyawan</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Karyawan</li>
        </ol>
    </div>
@endsection

@section('content')
    <div class="card sm mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary"></h6>
        </div>
        <div class="card-body">
            <a href="{{ route('employee.create') }}" class="btn btn-primary ml-3" style="cursor: pointer">Tambah
                Karyawan</a>
            <div class="table-responsive p-3">
                <table class="table align-items-center table-flush" id="myTable">
                    <thead class="thead-light">
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th>NIP</th>
                            <th>Tahun Lahir</th>
                            <th>Alamat</th>
                            <th>Nomor Hp</th>
                            <th>Agama</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($karyawan as $k)
                            <tr>
                                <td class="text-center">{{ $loop->iteration }}</td>
                                <td class="text-capitalize">{{ $k->nama }}</td>
                                <td>{{ $k->nip }}</td>
                                <td>{{ $k->tahun_lahir }}</td>
                                <td class="text-capitalize">{{ $k->alamat }}</td>
                                <td>{{ $k->nomor_hp }}</td>
                                <td class="text-capitalize">{{ $k->agama }}</td>
                                @if ($k->status == 0)
                                    <td>Tidak Aktif</td>
                                @else
                                    <td>Aktif</td>
                                @endif
                                <td>
                                    <a href="{{ route('employee.edit', $k->id) }}"
                                        class="btn btn-primary btn-sm mb-1">Edit</a>
                                    <form action="{{ route('employee.destroy', $k->id) }}" method="POST">
                                        @csrf
                                        @method('delete')
                                        <button class="btn btn-danger btn-sm">Hapus</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#myTable').DataTable({
                responsive: true
            });
        });
    </script>
@endpush
