@extends('layout.app')

@section('breadcrumb')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Tambah Karyawan</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Tambah Karyawan</li>
        </ol>
    </div>
@endsection

@section('content')
    <form action="{{ route('employee.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="nama">Nama</label>
                <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" id="nama" required>
                @error('nama')
                    <div class="alert alert-danger mt-2">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group col-md-6">
                <label for="nip">NIP</label>
                <input type="number" class="form-control @error('nip') is-invalid @enderror" name="nip" id="nip" required>
                @error('nip')
                    <div class="alert alert-danger mt-2">
                        {{ $message }}
                    </div>
                @enderror
            </div>
        </div>
        <div class="form-group">
            <label for="alamat">Alamat</label>
            <input type="text" class="form-control" name="alamat" id="alamat" required>
        </div>
        <div class="form-group">
            <label for="nomor_hp">Nomor Hp</label>
            <input type="number" class="form-control" name="nomor_hp" id="nomor_hp" required>
        </div>
        <div class="form-group">

            <label>Agama</label>
            <div class="form-check">
                <input class=" form-check-input" type="radio" name="agama" value="islam">
                <label class="form-check-label">
                    Islam
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="agama" value="kristen">
                <label class="form-check-label">
                    Kristen
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="agama" value="katolik">
                <label class="form-check-label">
                    Katolik
                </label>
            </div>
        </div>
        <div class="form-group">
            <label>Tahun Lahir</label>
            <select name="tahun_lahir" class="form-control">
                <option selected disabled>Pilih Tahun Lahir</option>
                <?php for ($i = 1975; $i <= 2018; $i++) : ?>
                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                <?php endfor; ?>
            </select>
        </div>
        <div class="form-group">
            <label>KTP</label>
            <div class="custom-file mb-3">
                <input type="file" class="custom-file-input @error('gambar') is-invalid @enderror" name="gambar"
                    id="gambar">
                <label class="custom-file-label">Pilih File</label>
                {{-- <div class="invalid-feedback">Example invalid custom file feedback</div> --}}

                @error('gambar')
                    <div class="alert alert-danger mt-2">
                        {{ $message }}
                    </div>
                @enderror
            </div>
        </div>
        <div class="form-group ">
            <label>Status</label>
            <select name="status" class="form-control">
                <option selected disabled>Pilih Status Karyawan</option>
                <option value="true">Aktif</option>
                <option value="0">Tidak Aktif</option>
            </select>
        </div>

        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
@endsection
